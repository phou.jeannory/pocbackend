package com.example.poc.service;

import com.example.poc.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> getUsers();

    User createUser(User user);

    User updateUser(UUID id, User user);

    void deleteUser(UUID id);
}
