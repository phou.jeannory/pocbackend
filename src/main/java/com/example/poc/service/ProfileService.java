package com.example.poc.service;

import com.example.poc.entity.Profile;

import java.util.List;

public interface ProfileService {

    List<Profile> getProfiles();
}
