package com.example.poc.service;

import com.example.poc.entity.Profile;
import com.example.poc.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileServiceImpl implements ProfileService{

    @Autowired
    private ProfileRepository profileRepository;

    @Override
    public List<Profile> getProfiles() {
        return profileRepository.findAll();
    }
}
