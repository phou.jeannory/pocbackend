package com.example.poc.repository;

import com.example.poc.entity.TravelTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TravelTimeRepository extends JpaRepository<TravelTime, UUID> {
}
