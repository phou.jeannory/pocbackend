package com.example.poc.dto;

import com.example.poc.entity.Profile;
import com.example.poc.enums.Right;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProfileDto {


    private UUID id;
    private String name;
    private List<Right> rights = new ArrayList<>();
    private String remark;

    public Profile toEntity(){
        return Profile
                .builder()
                .id(this.id)
                .name(this.name)
                .rights(this.rights)
                .remark(this.remark)
                .build();
    }
}
