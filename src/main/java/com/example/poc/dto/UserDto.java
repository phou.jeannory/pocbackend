package com.example.poc.dto;

import com.example.poc.entity.User;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {

    private UUID id;
    private String email;
    private String subject;
    private String lastName;
    private String firstName;
    private String phoneNumber;
    private List<ProfileDto> profiles = new ArrayList<>();
    private List<AddressDto> addresses = new ArrayList<>();

    public User toEntity(){
        return User
                .builder()
                .id(this.id)
                .email(this.email)
                .subject(this.subject)
                .lastName(this.lastName)
                .firstName(this.firstName)
                .phoneNumber(this.phoneNumber)
                .profiles(profiles.stream().map(ProfileDto::toEntity).collect(Collectors.toList()))
                .build();
    }
}
