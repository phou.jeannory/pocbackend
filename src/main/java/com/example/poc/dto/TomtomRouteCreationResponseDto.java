package com.example.poc.dto;

import com.example.poc.enums.TomtomRouteStatus;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class TomtomRouteCreationResponseDto {
    private int routeId;
    private String routeName;
    private TomtomRouteStatus routeStatus;
    private int routeLength;
    private List<TomtomRoutePointDto> pathPoints;
    private List<TomtomRoutePointDto> routedPathPoints;
}
