package com.example.poc.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class TomtomRouteCreationRequestDto {
    private String name;
    private List<TomtomRoutePointDto> pathPoints;
}
