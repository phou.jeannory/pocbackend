package com.example.poc.dto;

import com.example.poc.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddressDto {

    private UUID id;
    private String name;
    private double longitude;
    private double latitude;

    public Address toEntity(){
        return Address
                .builder()
                .id(this.id)
                .name(this.name)
                .longitude(this.longitude)
                .latitude(this.latitude)
                .build();
    }
}
