package com.example.poc.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class TomtomRoutePointDto {
    private double latitude;
    private double longitude;
}
