package com.example.poc.controller;

import com.example.poc.dto.ProfileDto;
import com.example.poc.dto.UserDto;
import com.example.poc.entity.Profile;
import com.example.poc.entity.User;
import com.example.poc.service.ProfileService;
import com.example.poc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/profiles")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @GetMapping
    public ResponseEntity<List<ProfileDto>> getProfiles() {
        return ResponseEntity.ok(profileService.getProfiles().stream().map(Profile::toDto).collect(Collectors.toList()));
    }

}
