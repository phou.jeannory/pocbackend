package com.example.poc.controller;

import com.example.poc.entity.*;
import com.example.poc.repository.*;
import com.example.poc.enums.Right;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/data")
public class DataController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private TravelTimeRepository travelTimeRepository;

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @Autowired
    private AddressRepository addressRepository;

    @GetMapping("/data-1")
    public String getData1() {

        final Profile profile1 = profileRepository.save(Profile
                .builder()
                .name("ADMIN")
                .rights(Arrays.asList(Right.ADMIN, Right.MANAGER, Right.USER))
                .build());

        final Profile profile2 = profileRepository.save(Profile
                .builder()
                .name("MANAGER")
                .rights(Arrays.asList(Right.MANAGER))
                .build());

        final Profile profile3 = profileRepository.save(Profile
                .builder()
                .name("USER")
                .rights(Arrays.asList(Right.USER))
                .build());

        final Address address1 = addressRepository.save(
                Address
                        .builder()
                        .name("address 1")
                        .longitude(2.3)
                        .latitude(48.8)
                        .build()
        );

        final Address address2 = addressRepository.save(
                Address
                        .builder()
                        .name("address 2")
                        .longitude(2.35)
                        .latitude(48.85)
                        .build()
        );

        final Address address3 = addressRepository.save(
                Address
                        .builder()
                        .name("address 3")
                        .longitude(2.36)
                        .latitude(48.95)
                        .build()
        );

        userRepository.save(User
                .builder()
                .email("jeanno.gmail.com")
                .subject("JEANNO")
                .profiles(List.of(profile1, profile2, profile3))
                .addresses(List.of(address1))
                .build());

        userRepository.save(User
                .builder()
                .email("jeff@gmail.com")
                .subject("JEFF_K")
                .profiles(List.of(profile2))
                .addresses(List.of(address2))
                .build());

        userRepository.save(User
                .builder()
                .email("john@gmail.com")
                .subject("JOHN_D")
                .profiles(List.of(profile3))
                .addresses(List.of(address3))
                .build());

        final TravelTime travelTime1 = travelTimeRepository.save(TravelTime
                .builder()
                .sca("ASFY")
                .sector("Sud")
                .axis(List.of("A19", "A18"))
                .build());

        final TravelTime travelTime2 = travelTimeRepository.save(TravelTime
                .builder()
                .sca("SCO")
                .sector("Nord")
                .axis(List.of("A86", "A6"))
                .build());

        final TravelTime travelTime3 = travelTimeRepository.save(TravelTime
                .builder()
                .sca("AMN")
                .sector("Est")
                .axis(List.of("A355"))
                .build());

        final TravelTime travelTime4 = travelTimeRepository.save(TravelTime
                .builder()
                .sca("ARN")
                .sector("Nord")
                .axis(List.of("C18", "C19"))
                .build());

        userSettingsRepository.save(UserSettings
                .builder()
                .subject("PHOU_J")
                .travelTimeFavorites(List.of(travelTime1.getId(), travelTime2.getId()))
                .build());

        userSettingsRepository.save(UserSettings
                .builder()
                .subject("JEFF_K")
                .travelTimeFavorites(List.of(travelTime3.getId(), travelTime4.getId()))
                .build());

        return "success";
    }
}
