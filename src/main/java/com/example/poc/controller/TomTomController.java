package com.example.poc.controller;

import com.example.poc.dto.TomtomRouteCreationRequestDto;
import com.example.poc.dto.TomtomRouteCreationResponseDto;
import com.example.poc.dto.TomtomRoutePointDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class TomTomController {

    @PostMapping(path = "/api/tomtom/routemonitoring/2/routes")
    public ResponseEntity<TomtomRouteCreationResponseDto> tomtomRouteCreationResponseDtoResponseEntity(
            @RequestBody TomtomRouteCreationRequestDto tomtomRouteCreationRequestDto,
            @RequestParam("key") String key
    ) {
        System.out.println("tomtom start");
        System.out.println("tomtomRouteCreationRequestDto " + tomtomRouteCreationRequestDto);
        System.out.println("key " + key);
        return ResponseEntity.ok(TomtomRouteCreationResponseDto
                .builder()
                .pathPoints(List.of(TomtomRoutePointDto
                        .builder()
                        .latitude(51.76041)
                        .longitude(19.4721)
                        .build()))
                .build());
    }
}
