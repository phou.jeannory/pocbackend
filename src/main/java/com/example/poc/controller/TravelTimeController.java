package com.example.poc.controller;

import com.example.poc.repository.TravelTimeRepository;
import com.example.poc.entity.TravelTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/travel-times")
public class TravelTimeController {

    @Autowired
    private TravelTimeRepository travelTimeRepository;

    @GetMapping
    public ResponseEntity<List<TravelTime>> getAllTravelTimes() {
        return ResponseEntity.ok(travelTimeRepository.findAll());
    }
}
