package com.example.poc.controller;

import com.example.poc.repository.UserSettingsRepository;
import com.example.poc.entity.UserSettings;
import com.example.poc.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/user-settings")
public class UserSettingsController {

    @Autowired
    private UserSettingsRepository userSettingsRepository;

    @GetMapping
    public ResponseEntity<List<UserSettings>> getAllUserSettings(){
        return ResponseEntity.ok(userSettingsRepository.findAll());
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserSettings> findUserSettings(@PathVariable UUID userId){
        return ResponseEntity.ok(userSettingsRepository.findById(userId).orElseThrow(NotFoundException::new));
    }

    @GetMapping(path = "/subject/{subject}")
    public ResponseEntity<UserSettings> findUserSettingsBySubject(@PathVariable String subject){
        return ResponseEntity.ok(userSettingsRepository.findBySubject(subject).orElseThrow(NotFoundException::new));
    }

    @PutMapping(path = "/favorites/travel-time/{subject}")
    public ResponseEntity<UserSettings> updateFavoritesTravelTimes(@PathVariable String subject, @RequestBody List<UUID> uuids){
        final UserSettings userSettings = userSettingsRepository.findBySubject(subject).orElseThrow(NotFoundException::new);
        userSettings.setTravelTimeFavorites(uuids);
        return ResponseEntity.ok(userSettingsRepository.save(userSettings));
    }
}
