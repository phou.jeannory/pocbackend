package com.example.poc.entity;

import com.example.poc.dto.ProfileDto;
import com.example.poc.enums.Right;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Profile {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Getter
    private UUID id;
    @Column(nullable = false, unique = true)
    @Getter
    private String name;
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "profile_rights", joinColumns = @JoinColumn(name = "profile_id"))
    @Enumerated(EnumType.STRING)
    @Getter
    private List<Right> rights = new ArrayList<>();
    @Getter
    private String remark;
    @ManyToMany(mappedBy = "profiles")
    private List<User> users = new ArrayList<>();

    public ProfileDto toDto(){
        return ProfileDto
                .builder()
                .id(id)
                .name(name)
                .rights(rights)
                .remark(remark)
                .build();
    }
}
