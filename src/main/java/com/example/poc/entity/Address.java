package com.example.poc.entity;

import com.example.poc.dto.AddressDto;
import com.example.poc.dto.ProfileDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Getter
    private UUID id;
    private String name;
    private double longitude;
    private double latitude;

    @ManyToMany(mappedBy = "addresses")
    private List<User> users = new ArrayList<>();

    public AddressDto toDto(){
        return AddressDto
                .builder()
                .id(id)
                .name(name)
                .longitude(longitude)
                .latitude(latitude)
                .build();
    }
}
