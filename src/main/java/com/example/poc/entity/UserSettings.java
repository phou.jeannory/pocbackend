package com.example.poc.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserSettings {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NaturalId
    private String subject;

    @Setter
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "user_settings_travel_time_favorite", joinColumns = @JoinColumn(name = "user_settings_id"))
    private List<UUID> travelTimeFavorites = new ArrayList<>();

}
