package com.example.poc.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TravelTime {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private String sca;
    private String sector;
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "travel_time_axis", joinColumns = @JoinColumn(name = "travel_time_id"))
    private List<String> axis = new ArrayList<>();
}
