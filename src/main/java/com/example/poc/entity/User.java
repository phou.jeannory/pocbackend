package com.example.poc.entity;

import com.example.poc.dto.UserDto;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name="user_vinci")
public class User {

    @Setter
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false, unique = true)
    private String subject;
    private String lastName;
    private String firstName;
    private String phoneNumber;
    @ManyToMany
    @JoinTable(
            name = "user_profile",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "profile_id"))
    private List<Profile> profiles = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "user_address",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "address_id"))
    private List<Address> addresses = new ArrayList<>();

    public UserDto toDto(){
        return UserDto
                .builder()
                .id(id)
                .email(email)
                .subject(subject)
                .lastName(lastName)
                .firstName(firstName)
                .phoneNumber(phoneNumber)
                .profiles(profiles.stream().map(Profile::toDto).collect(Collectors.toList()))
                .addresses(addresses.stream().map(Address::toDto).collect(Collectors.toList()))
                .build();
    }
}
