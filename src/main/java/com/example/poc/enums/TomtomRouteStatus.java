package com.example.poc.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TomtomRouteStatus {
    NEW("NEW"),
    ACTIVE("ACTIVE"),
    PENDING_UPDATE("PENDING_UPDATE"),
    MM_FAILED("MM_FAILED"),
    ARCHIVED("ARCHIVED");

    private final String code;

    TomtomRouteStatus(String code) {
        this.code = code;
    }

    @JsonValue
    public String getCode() {
        return code;
    }
}
