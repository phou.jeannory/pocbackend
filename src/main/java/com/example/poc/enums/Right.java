package com.example.poc.enums;

public enum Right {
    USER("user"),
    MANAGER("manager"),
    ADMIN("admin");

    private final String code;

    Right(String code) { this.code = code; }

    public String getCode() {
        return code;
    }
}
